public interface HidingPlaces {
    void hideEgg();
}

class BasketPlace implements HidingPlaces{
@Override
public void hideEgg() {
    System.out.println("Egg is hidden in the basket");
    }
}

class ForestPlace implements HidingPlaces{
    @Override
    public void hideEgg() {
        System.out.println("Egg is hidden in the forest");
    }
}

class BushPlace implements HidingPlaces{
    @Override
    public void hideEgg() {
        System.out.println("Egg is hidden in the bush");
    }
}
