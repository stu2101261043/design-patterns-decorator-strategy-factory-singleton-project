public class EggMaker {

    public static Egg createEgg(String eggType){
        switch(eggType){
            case "chicken":
                System.out.println("Chicken egg is created!");
                return new ChickenEgg();
            case "dino":
                System.out.println("Dino egg is created!");
                return new DinoEgg();
            case "ostrich":
                System.out.println("Ostrich egg is created!");
                return new OstrichEgg();

            default: return null;
        }
    }

}
