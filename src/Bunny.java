public class Bunny {    //singleton
    private static Bunny instance;

    private Bunny(){}

    public static Bunny getInstance(){      //Class instance to call the bunny
        if(instance == null){
            instance = new Bunny();
        }
        System.out.println("The bunny has been released");
        return instance;
    }

    public void colorDecorate(Egg egg, String color){
        System.out.println("The " + egg.nameToString() + " is colored " + color);
    }

    public void stickerDecorate(Egg egg, String stickerType){
        System.out.println("The " + egg.nameToString() + " has a " + stickerType + " sticker");
    }
}
