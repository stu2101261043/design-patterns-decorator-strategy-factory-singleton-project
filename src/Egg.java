public abstract class Egg {
    private String color;
    private boolean isDecorated;

    void hideEgg() {
        System.out.println("Egg is hidden");
    }

    void decorateEgg() {
        System.out.println("Egg is decorated");
    }

    public boolean isDecorated() {
        return isDecorated;
    }

    public void setDecorated(boolean decorated) {
        isDecorated = decorated;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String nameToString(){   //translates the code to string
        return this.getClass().getSimpleName();
    }
}
