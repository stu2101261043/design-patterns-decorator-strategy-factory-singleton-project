public class OstrichEgg extends Egg {
    @Override
    public void hideEgg() {
        System.out.println("Ostrich egg is hidden");
    }

    @Override
    public void decorateEgg() {
        System.out.println("Ostrich egg is decorated");
    }
}
