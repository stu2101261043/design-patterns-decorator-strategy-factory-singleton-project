public class DinoEgg extends Egg {
    @Override
    public void hideEgg() {
        System.out.println("Dino egg is hidden");
    }

    @Override
    public void decorateEgg() {
        System.out.println("Dino egg is hidden");
    }
}
