public class ChickenEgg extends Egg {
    @Override
    public void hideEgg() {
        System.out.println("Chicken egg is hidden");
    }

    @Override
    public void decorateEgg() {
        System.out.println("Chicken egg is decorated");
    }
}
