public abstract class EggDecorator extends Egg {
    protected Egg decoratedEgg;

    public EggDecorator(Egg decoratedEgg){
        this.decoratedEgg = decoratedEgg;
    }

    public void decorateEgg(){
        decoratedEgg.decorateEgg();
    }

    public void hideEgg(){
        decoratedEgg.hideEgg();
    }

    public static class ColorDecor extends EggDecorator{
        private final String eggColor;


        public ColorDecor(Egg decoratedEgg,String eggColor){
            super(decoratedEgg);
            this.eggColor = eggColor;

        }

        @Override
        public void decorateEgg(){
            super.decorateEgg();
            this.decoratedEgg.decorateEgg();
            this.addColor();
        }

        public void addColor(){
            System.out.println("Added color: " + this.eggColor);
        }
    }

    public class StickerDecor extends EggDecorator{
        private final String sticker;
        public StickerDecor(Egg decoratedEgg, String sticker){
            super(decoratedEgg);
            this.sticker = sticker;
        }

        public void decorateEgg(){
            super.decorateEgg();
            this.addSticker();
        }

        private void addSticker(){
            System.out.println("Sticker added: " + this.sticker);
        }
    }

}
