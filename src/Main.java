public class Main {
    public static void main(String[] args) {

        Bunny easterBunny = Bunny.getInstance();    //calls the bunny

        EggMaker eggMaker = new EggMaker(); //calls the egg factory

        Egg dinoEgg = EggMaker.createEgg("dino");
        Egg chickenEgg = EggMaker.createEgg("chicken");
        Egg ostrichEgg = EggMaker.createEgg("ostrich");

        Egg coloredChickenEgg = new EggDecorator.ColorDecor(chickenEgg,"red");

        easterBunny.colorDecorate(chickenEgg,"red");

        easterBunny.colorDecorate(dinoEgg,"purple");
        easterBunny.stickerDecorate(dinoEgg,"star");
//        HidingPlaces hidingPlaceForest = new ForestPlace();
//
//
//
//        ostrichEgg.decorateEgg();
//
//        DinoEgg.hideEgg();
//        coloredChickenEgg.hideEgg();

    }
}
